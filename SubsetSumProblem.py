from random import randint, sample
import random
import itertools
import time
import math
import sys
import numpy as np
# uncomment to print full untruncated matrix
# np.set_printoptions(threshold=np.nan)

class SSP():
    #initializing the SSP object
    def __init__(self, S=[], t=0):
        self.S = S #empty set to  start with
        self.t = t #target set to 0 initially
        self.n = len(S) #length of the set (i.e. number of elements)
        #
        self.decision = False 
        self.total    = 0
        self.selected = [] #empty set for the selected subset

    def __repr__(self):
        return "SSP instance: S="+str(self.S)+"\tt="+str(self.t)
    
    def random_instance(self, n, bitlength=10):
        max_n_bit_number = 2**bitlength-1
        self.S = sorted( [ randint(0,max_n_bit_number) for i in range(n) ])
        self.t = randint(0,n*max_n_bit_number)
        self.n = len( self.S )

    #generate random SSP instance which will generate a solution
    def random_yes_instance(self, n, bitlength):
        max_n_bit_number = 2**bitlength-1
        #generate a random set of integers of length n
        #and store as reverse sorted
        self.S = sorted( [ randint(0,max_n_bit_number) for i in range(n) ], reverse=True)
        #set the target value to the sum of a sample subset of the set
        #where the length of the sample set is a randint between 0 and n (4)
        self.t = sum( sample(self.S, randint(0,n)) )
        # self.t = sum(self.S)
        #set n property of object to length of the set 
        self.n = len( self.S )
        
    #try generating a solution randomly (greedy algorithm)
    def try_at_random(self):
        #initialise a empty set for the candidate
        candidate = []
        #current total of sum of subset
        total = 0
        #whilst the total sum is not equal to target keep trying
        while total != self.t:
            #get a random sample of between 0 and 4 elements to test
            candidate = sample(self.S, randint(0,self.n))
            #evaluate the total of the subset
            total     = sum(candidate)
            # print( "Trying: ", candidate, ", sum:", total )

    
    #function to perform the brute force         
    def brute_force(self):
        #calls the function getSubsets to generate all the subsets for the initial set and return the subsets
        subsets =  self.getSubsets()
        total = 0
        #loop through every set of the subsets
        start = time.time()
        for set in subsets:
            #set the total to equal the sum of the current set
           total = sum(set)
           #print the set that is currently being tested
           # print("Trying: ", set, ", sum:", total )
           #if the total does not match the value of t which is the target value then solution has been found
           if (total == self.t):
                #print the solution out
                # print("Solution: " +str(set))
                end = time.time()
                # print "Timer: " + '{0:.10f}'.format(end-start)
                final = '{0:.10f}'.format(end-start)
                return final

    def DynamicProgramming(self):
        
        set = np.full((self.t+1,self.n+1),-1)
        # print(set)
        for i in range(0,self.n+1):
            set[0][i] = True

        for j in range(1, self.t+1):
            set[j][0] = False

        start = time.time()
        for i in range(1, self.t+1):
            for j in range(1, self.n+1):
                set[i][j] = set[i][j-1]
                if (i >= self.S[j-1]):
                    set[i][j] = set[i][j] or set[i - self.S[j-1]][j-1]
        # print str(set)
        end = time.time()
        final = '{0:.10f}'.format(end-start)
        return final
        # return set[self.t][self.n]


    def AcceptanceProbability(self, new, old, temp): 
        if new or old is not None:
            if (new < old):
                return 1.0
            else:
                return math.exp((old-new)/temp)
        else:
            return 0


    # def GRASP(self, max_iter):
    #     best_candidate = 0
    #     while (i <=  max_iter):
    #         greedy_candidate = GreedyAlgorithm()
    #         grasp_candidate = LocalSearch(greedy_candidate)


    # def LocalSearch(self,greedy_set):
    #     candidate = []
    #     for i in range(0, self.n):
    #         for j in range(0, greedy_set):
    #             if (self.S[i] not in greedy_set):
    #                 greedy_set[i]

    def NeigbouringSolution(self, set = []):
        if len(set) != 0:
            randPosSet = randint(0, len(self.S)-1)
            randPosSet1 = randint(0, len(set)-1)
            # print randPosSet
            # print str(randPosSet1) + "\n"
            set1 = list(set)
            
            if len(self.S) == len(set1):
                print "No Neighbouring Solution"
            else:
                while (self.S[randPosSet] in set1):
                    randPosSet = randint(0, len(set))  
                    
                set1[randPosSet1] = self.S[randPosSet]
                
            print("Initial: " +str(set)) 
            print("Neighbour: " +str(set1) + "\n")
            return set1

    def CalculateCost(self,set):
        if not set:
            return
        else:   
            return sum(set)

    def SimAnneal(self):
        solution = self.GreedyAlgorithm()
        old_cost = self.CalculateCost(solution)
        print str(solution) + " " + "Cost: " +str(old_cost)
        print "old cost: " + str(old_cost)
        Temp = 100
        TempMin = 1
        alpha = 0.9

        if len(solution) == 0 or old_cost == self.t:
            return

        while Temp > TempMin:
            i = 1
            while i <= 1:
                # print("i: " + str(i))
                solution1 = self.NeigbouringSolution(solution)
                # print("Initial Solution: " +str(solution) + " Cost: " + str(old_cost))
                new_cost = self.CalculateCost(solution1)
                # print("Neighbouring Solution: " +str(solution1)  + " Cost: " + str(new_cost))
                # print("new cost: " + str(new_cost))
                ap = self.AcceptanceProbability(old_cost, new_cost,Temp)
                # print "ap: " + str(ap)
                randomnum = random.random()
                # print "random: " +str(randomnum) + "\n"
                if ap > randomnum and sum(solution1) <= self.t:
                    solution = list(solution1)
                    print("NEW SOLUTION: " +str(solution))
                    old_cost = new_cost
                i = i + 1
            Temp = Temp*alpha
        return str(solution), sum(solution)

    def GreedyAlgorithm(self):
        total = 0
        subsets = []
        start = time.time()
        for i in range(0, len(self.S)):
            if (sum(subsets) + self.S[i]<= self.t):
                subsets.append(self.S[i])
                total = total + self.S[i]
            else:
                print(total-self.t)
                break
        print ("Greedy: " +str(subsets))
        end = time.time()
        final = '{0:.10f}'.format(end-start)
        # return abs(total-self.t)
        return subsets

    #function to generate all the subsets of the initial set
    def getSubsets(self):
        #empty array to store all the subsets, len will be equal to 2^n where n is the length of the initial set
        results = []
        #loops through the length of the set and generates a non repeated array of subsets
        for i in range(0,len(self.S)+1):
            #use combinatorics to find all possible non repeated combinations (subsets) of the initial set 
            results += itertools.combinations(self.S,i)

        # #loop through the results array and print all the subsets
        # for x in results:
        #     print(list(x))

        return results

# testing for the greedy algorithm
# for j in range(4,100):
#     # print "Length: " + str(j)
#     totalTime = 0.0
#     for i in range(0,10000):
#         instance = SSP() #generate instance of SSP Class and initialise
#         instance.random_instance(j)
#         totalTime = totalTime + float(instance.GreedyAlgorithm())
#     avg = totalTime/10000
#     print avg        


# dynamic test
# for j in range(0,10):
#     # print "Length: " + str(j)
#     totalTime = 0.0
#     for i in range(0,100):
#         instance = SSP() #generate instance of SSP Class and initialise
#         instance.random_yes_instance(4, j)
#         # print( instance)
#         totalTime = totalTime + float(instance.DynamicProgramming())
#     avg = totalTime/2000
#     print avg



#quality of approx test
# for j in range(0,100):
#     # print "Length: " + str(j)
#     totalTime = 0
#     t = 0
#     for i in range(0,10000):
#         instance = SSP() #generate instance of SSP Class and initialise
#         instance.random_instance(j)
#         t = instance.t
#         # print( instance)
#         totalTime = totalTime + int(instance.GreedyAlgorithm())
#     avg = totalTime/10000
#     if t == 0:
#         print 100
#     else:
#         ratio = (avg/t)*100
#         print ratio


# brute force testing
# instance = SSP()
# for i in range(0, 25):
#     print "Length: " + str(i)
#     totalTime = 0.0
#     for j in range(0,101):
#         instance.random_yes_instance(i)
#         # instance.brute_force()
#         totalTime = totalTime + float(instance.brute_force())
#     avg = totalTime/101
#     print avg
# print (instance)

